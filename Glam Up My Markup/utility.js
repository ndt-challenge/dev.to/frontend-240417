const fetchrule = { cache: 'no-cache', credentials: 'omit' }

const maybe_quotes = fetch('res/quotes.json', { ...fetchrule, cache: 'force-cache' }).then(r => r.json())

export function randbin(plus, minus)
{
   return Math.random() < 0.5 ? plus : minus
}

export function rand(max, min = 0, after_decimal_sz = 0)
{
   const tenth = Math.pow(10, after_decimal_sz)
   const randval = Math.random() * (max - min + 1) + min
   return Math.floor(randval * tenth) / tenth
}

export async function randquote()
{
   const quotes = await maybe_quotes
   return quotes[rand(quotes.length - 1)]
}

export function animate(cb, fps = 60)
{
   let then, cancelled_id
   const fps_interval = 1000 / fps

   function step(now)
   {
      if (then === undefined) then = now

      const elapsed = now - then
      if (elapsed < fps_interval)
      {
         cancelled_id = window.requestAnimationFrame(step)
         return void 0
      }

      cb(cancelled_id)
      then = now
      cancelled_id = window.requestAnimationFrame(step)
   }

   cancelled_id = window.requestAnimationFrame(step)
}

export function tick_text(unique_selector, char, fps)
{
   let inx = 0
   animate(_ =>
   {
      if (char.length === inx) inx = 0
      document.querySelector(unique_selector).innerText = char[inx++]
   }, fps)
}

export function tick_globe()
{
   tick_text('#tick-globe', ['🌍', '🌏', '🌎'], 4)
}

export async function geolocation()
{
   const geoopt = {
      timeout: 5000,
      maximumAge: 9999999,
   }
   const maybe_geo = await new Promise(
      (yes, no) => navigator.geolocation?.getCurrentPosition(yes, no, geoopt)
   )

   return {
      latitude: maybe_geo.coords.latitude || 0,
      longitude: maybe_geo.coords.longitude || 0
   }
}

export async function weather()
{
   const base = 'https://api.open-meteo.com/v1/forecast'

   const { latitude, longitude } = await geolocation()
   const opt = new URLSearchParams({ latitude, longitude })

   return {
      /**
       * 
       * @param { 2 | 80 | 120 | 180 } minutes 
       * @returns {Promise<{ 
       *   time: string,
       *   interval: number,
       *   temperature_2m: number,
       *   temperature_80m: number,
       *   temperature_120m: number,
       *   temperature_180m: number,
       * }>}
       */
      async temperature(minutes = 180)
      {
         if (![2, 80, 120, 180].includes(minutes)) { minutes = 180 }
         opt.set('current', `temperature_${minutes}m`)

         const res = await fetch(
            new URL('?' + opt.toString(), base),
            fetchrule
         )
         const { current } = await res.json()
         return current
      }
   }
}

export async function get_temperature()
{
   const debug = localStorage.getItem('temperature')
   if (debug) return parseFloat(debug)

   try
   {

      const { temperature } = await weather()
      const { temperature_180m } = await temperature()

      return temperature_180m
   }
   catch (_e)
   {
      return 69
   }

}
