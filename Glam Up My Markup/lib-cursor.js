import { animate, rand, randbin } from './utility.js'

/**
 * @typedef { object } Particle
 * @property { string } val
 * @property { number } x
 * @property { number } y
 * @property { number } sz
 * @property { number } alpha
 * @property {{ x: number, y: number }} velocity
 * @property { 'line' | 'round' } type
 */

/**
 * Inspired by 
 *   - 90's emoji cursor of Tim Holman
 *   - Mouse Trail with Canvas of Lebo Mawaya
 * @credit https://codepen.io/tholman/pen/rxJpdQ?editors=0010
 * @credit https://dev.to/mawayalebo/creating-an-interactive-glowing-mouse-trail-with-html5-canvas-and-javascript-4a04
 */
export class TrailingCursor extends HTMLElement
{
   static observedAttributes = ['char']

   /** @type string[] */
   #charls = ['🔥', '🌊', '🍃', '☘️', '😎', '🇻🇳']

   /** @type Particle[] */
   #particles = []

   /** @type number */
   #max_ptc = 90

   /** @type ShadowRoot */
   #shadow

   /** @type HTMLCanvasElement */
   #canvas

   /** @type CanvasRenderingContext2D */
   #ctx

   /** @type { number | undefined } Request Animation Frame ID */
   #aid

   get ctx()
   {
      return this.#ctx
   }

   constructor(char)
   {
      super()
      this.#update_charls(char)
      this.#shadow = this.attachShadow({ mode: 'closed' })
      this.#canvas = document.createElement('canvas')
      this.#ctx = this.#canvas.getContext('2d')
      this.#shadow.append(this.#canvas)

      this.#window_resize()
   }

   /**
    * @param { string | string[] } input 
    * @param { boolean } is_silent 
    */
   #update_charls(input, is_silent = false)
   {
      if (Array.isArray(input))
      {
         this.#charls = input
      }
      else if (typeof input === 'string')
      {
         this.#charls = [...input]
      }
      else
      {
         if (is_silent) return false
         else throw new Error('[TrailingCursor] Invalid data type: either a string or an array of string type.')
      }
   }

   /** @param { number } aid */
   #draw(aid)
   {
      this.#aid = aid
      if (this.#particles.length === 0) return void 0

      this.ctx.clearRect(0, 0, this.#canvas.width, this.#canvas.height)
      this.ctx.save()

      for (let i = 0; i < this.#particles.length; i++)
      {
         const particle = this.#particles[i]

         if (particle.alpha <= -0.1 || particle.sz <= 0)
         {
            this.#particles.splice(i, 1)
            continue
         }

         this.ctx.fillStyle = `rgba(0,0,0,${particle.alpha})`
         particle.alpha -= 0.01

         this.ctx.font = `${particle.sz}rem mono`
         particle.sz -= 0.01

         this.ctx.fillText(particle.val, particle.x, particle.y)
         particle.x += particle.velocity.x
         particle.y += particle.velocity.y
         this.ctx.restore()
      }
   }

   #new_particle(x, y, sz, alpha = 1, type = 'line')
   {
      if (this.#charls.length === 0) return void console.warn('[TrailingCursor] Warning, the element stops drawing due to no trailing character found.')

      const velocity = type === 'line'
         ? {
            x: randbin(-3, 3) * Math.random(),
            y: 7 * Math.random(),
         }
         : {
            x: randbin(-7, 7) * Math.random(),
            y: randbin(-7, 7) * Math.random(),
         }
      this.dispatchEvent(new CustomEvent(
         'particle-velocity/new',
         {
            detail: {
               type,
               velocity,
            }
         }
      ))

      const inx = this.#charls.length === 1 ? 0 : rand(this.#charls.length - 1)
      this.#particles.push({
         val: this.#charls[inx],
         x, y,
         sz, alpha,
         velocity,
         type,
      })

      if (this.#particles.length > this.#max_ptc) this.#particles.shift()
   }

   /** @param { UIEvent } _e */
   #window_resize(_e)
   {
      const { width, height } = document.body.getBoundingClientRect()
      this.#canvas.width = width
      this.#canvas.height = height
   }

   /** @param { MouseEvent } e */
   #cursor_click(e)
   {
      for (let c = 0; c < 5; c++) this.#new_particle(
         e.pageX, e.pageY,
         rand(0.7, 0.5),
         0.7,
         'round'
      )
   }

   /** @param { WheelEvent } e */
   #cursor_wheel(e)
   {
      this.#new_particle(e.pageX, e.pageY, rand(1, 0.9), 1)
   }

   /** @param { MouseEvent } e */
   #cursor_move(e)
   {
      this.#new_particle(e.pageX, e.pageY, rand(1, 0.9), 1)
   }

   /** @param { TouchEvent } e */
   #finger_touch(e) { }

   /** @param { TouchEvent } e */
   #finger_sweep(e) { }

   #update_style()
   {
      const sheet = new CSSStyleSheet()
      this.#shadow?.adoptedStyleSheets.push(sheet)
      sheet.replace(`
         :host
         {
            --z-index: 9;
            pointer-events: none;
            user-select: none;
            position: absolute;
            top: 0; left: 0;
            height: 0;
            z-index: var(--z-index);
         }
         canvas
         {
            display: block;
         }
      `)
      this.ctx.font = '1rem mono'
   }

   connectedCallback()
   {
      this.#update_style()

      window.addEventListener('resize', this.#window_resize.bind(this))
      document.body.addEventListener('wheel', this.#cursor_wheel.bind(this))
      document.body.addEventListener('click', this.#cursor_click.bind(this))
      document.body.addEventListener('mousemove', this.#cursor_move.bind(this))
      document.body.addEventListener('touchend', this.#finger_touch.bind(this))
      document.body.addEventListener('touchmove', this.#finger_sweep.bind(this))

      animate(this.#draw.bind(this), 45)
   }

   disconnectedCallback()
   {
      this.#aid ?? window.cancelAnimationFrame(this.#aid)

      window.removeEventListener('resize', this.#window_resize.bind(this))
      document.body.removeEventListener('wheel', this.#cursor_wheel.bind(this))
      document.body.removeEventListener('click', this.#cursor_click.bind(this))
      document.body.removeEventListener('mousemove', this.#cursor_move.bind(this))
      document.body.removeEventListener('touchend', this.#finger_touch.bind(this))
      document.body.removeEventListener('touchmove', this.#finger_sweep.bind(this))
   }

   /**
    * @param { string } name 
    * @param { string } _old_val 
    * @param { string } new_val 
    */
   attributeChangedCallback(name, _old_val, new_val)
   {
      if (name !== 'char') return void 0
      this.#update_charls(new_val.split(' '), true)
   }
}

customElements.define('trailing-cursor', TrailingCursor)