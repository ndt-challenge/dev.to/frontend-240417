import { randquote, tick_globe, tick_text } from './utility.js'



// 🌏 head > title
tick_text(
   'head > title',
   ['Earth Day: 🌏', 'Earth Day: 🌎', ' Earth Day: 🌍'],
   4
)


// 🌏 body > header
const header = document.querySelector('body > header')
const header_h1 = header.querySelector('h1')
header_h1.innerHTML = header_h1.textContent.replace(
   'Our Earth Day',
   '<span class="tx-leaf tx-outline nobreak animate-badump">Our Earth Day</span>'
)


// 🌏 .facts
const fact_content = document.querySelector('.facts > p')
fact_content.innerHTML = fact_content.textContent.replace(
   'EARTHDAY.ORG',
   '<a href="https://www.earthday.org" target="_blank">EARTHDAY.ORG</a>'
)


// 🌏 .action-call
const cta = document.querySelector('.action-call > a')
cta.href = 'https://www.earthday.org/earth-day-quizzes'
cta.target = '_blank'


// 🌏 .testimonial
const quote_holder = document.querySelector('.testimonial > p:nth-child(2)')
const quote = document.createElement('blockquote')
const quote_content = ({ msg, author }) => quote.innerHTML = `
   <p id='quote-msg'>${msg}</p>
   <footer id='quote-author'>${author}</footer>
`
quote.addEventListener('click', async _ =>
{
   const q = await randquote()
   quote_content(q)
})
randquote().then(quote_content).then(
   _ => quote_holder.replaceWith(quote)
)


// 🌏 .events
const events = document.querySelector('.events')
const ev_a = events.querySelector('p > a[href="#"]')
ev_a.href = 'https://www.earthday.org/earth-day-2024/#map'
ev_a.target = '_blank'
const ev_img = document.createElement('img')
ev_img.src = './res/party.webp'
events.appendChild(ev_img)


// 🌏 body > footer
const footer = document.querySelector('body > footer')
const footer_content = document.querySelector('body > footer > p')
   .textContent
   .split('! ')
const footer_title = footer_content[0]
   .replace('Earth', '<span id="tick-globe">🌏</span>')
footer.innerHTML = `
   <em>${footer_title}!</em>
   <p>${footer_content[1]}</p>
`
tick_globe()
