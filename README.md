# The Frontend Challenge: Earth Day Edition

(**17-April-2024** to **28-April-2024**) |
[Article](https://dev.to/devteam/join-us-for-the-next-frontend-challenge-earth-day-edition-52e4)
| [Info](https://dev.to/challenges/frontend-2024-04-17)

> [27-Apr-2024 GTM+7] Since I didn't join the CSS Art. It's it for me.
> The repo will be frozen. Any further code fixing will be done in `aftermath` branch.
> If you ever see one, you know what happens.

![](./thumbnail.png)

## The Prompts

For this time, we’ll be celebrating our planet’s largest civic event: Earth Day.

> 🌍 🌏 🌎

1. CSS Art: Earth Day (missed)
   - Draw what comes to mind for you when it comes to Earth Day. Is it our
     beautiful planet? Is it a symbol that drives awareness about climate
     change? Whatever it is, show us!
   - ~~[code](/%20CSS%20Art/)~~
   - ~~[demo](https://ndt-challenge.gitlab.io/dev.to/frontend-240417/css-art)~~
   - ~~[submission link](https://dev.to/ngdangtu/)~~
   - ~~[local](http://localhost:4321/%20CSS%20Art/art.html)~~
2. Glam Up My Markup: Earth Day Celebration Landing Page
   - Use CSS and JavaScript to make the below starter HTML markup beautiful,
     interactive, and useful.
   - [code](/Glam%20Up%20My%20Markup/)
   - [demo](https://ndt-challenge.gitlab.io/dev.to/frontend-240417/glam-up-my-markup)
   - [submission link](https://dev.to/ngdangtu/glam-up-my-markup-earth-day-celebration-landing-page-4nnp)
   - [local](http://localhost:4321/Glam%20Up%20My%20Markup/glamup.html)

To use **local** link, see the section below.

## Local Development

Deno is used for simplicity:

```shell
deno run --allow-net --allow-read https://deno.land/std@0.220.1/http/file_server.ts -p 4321
```

## Credit

- `/public/favicon.ico` (production) : https://dev.to
- `/favicon.ico` (development) :
  https://favicon.io/emoji-favicons/globe-showing-asia-australia

### Glam Up My Markup

- Media
   - `texture-stardust.png`
      + Atle Mo
      + https://www.transparenttextures.com/stardust.html
   - `texture-cloud.png`
      + Kerstkaarten
      + https://www.transparenttextures.com/nice-snow.html
   - `texture-leaf.png`
      + Lasma
      + https://www.transparenttextures.com/xv.html
   - `texture-bark.png`
      + Derek Ramsey
      + https://www.transparenttextures.com/shley-tree-1.html
   - `texture-solid.png`
      + Hendrik Lammers
      + https://www.transparenttextures.com/buried.html
   - `leaf.jpg`
      + modified
      + ZEE ー・ニウス
      + https://unsplash.com/photos/fill-the-frame-photography-of-green-leaves-dBq8TLV0K1k
   - `party.jpg`
      + Pineapple Supply Co.
      + https://unsplash.com/photos/several-pineapples-at-a-party-qWlkCwBnwOE
- API
   - Get anonymous location » http://ip-api.com
   - Get temperature » https://open-meteo.com
- `lib-cursor.js` See the code file for detail
   - https://codepen.io/tholman/pen/rxJpdQ?editors=0010
   - https://dev.to/mawayalebo/creating-an-interactive-glowing-mouse-trail-with-html5-canvas-and-javascript-4a04
